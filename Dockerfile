FROM openjdk:8-jdk-alpine
WORKDIR /hello-world-springboot-application
COPY build/libs/hello-world-0.1.0.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
